--holds worlds and its functions

function drawWorld(world)

    for i in pairs(world) do
        --yvalue
        for j in pairs(world[i]) do
            --xvalue
            if world[i][j] == 1 then
                love.graphics.rectangle("fill",j*16+1,i*16+1, 14, 14)--grid of 16x16)
            end
            
        end
    end
end

world1 = {
    {1,1,1,1,1,1,1,1},
    {1,0,0,0,0,0,0,1},
    {1,0,0,0,0,0,0,1},
    {1,0,0,0,0,1,1,1},
    {1,0,0,0,0,1,1,1},
    {1,0,0,0,0,0,0,1},
    {1,0,0,0,0,0,0,1},
    {1,1,1,1,1,1,1,1}
}

