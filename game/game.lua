function GameInitialize()
    require ("game/player")
    require ("game/worlds/worldmanager")
    player = Player(50,50) 
end

function GameUpdate(dt)
    player:Update(dt)
end

function Gamekeypressed(key)
    player:OnKeyPress()
end

function GameDraw()
    drawWorld(world1)
    player:Draw()
end