Player = Object:extend()

function Player:new(x,y)

    
    self.x = x
    self.y = y

    self.height = 16
    self.width = 16

    self.speed = 80

    self.rotation = 0
    self.rotateSpeed = 150
    self.lineLength = 15 --debug

end

function Player:Update(dt)
    if love.keyboard.isDown("s") then
        self.x = self.x + math.cos(degToRad(self.rotation + 90)) * self.speed * dt
        self.y = self.y - math.sin(degToRad(self.rotation + 90)) * self.speed * dt
    end

    if love.keyboard.isDown("w") then
        self.x = self.x - math.cos(degToRad(self.rotation + 90))* self.speed * dt
        self.y = self.y + math.sin(degToRad(self.rotation + 90))* self.speed * dt
    end

    if love.keyboard.isDown("a") or love.keyboard.isDown("left") then
        self.rotation = self.rotation + (dt * self.rotateSpeed)
    end

    if love.keyboard.isDown("d") or love.keyboard.isDown("right") then
        self.rotation = self.rotation - (dt * self.rotateSpeed)
    end

end

function Player:Draw()
    love.graphics.setColor(1,0,1,1)
    love.graphics.rectangle("fill", self.x-8, self.y-8, self.width, self.height)
    love.graphics.line(self.x, self.y, self.x+math.sin(degToRad(self.rotation)) * self.lineLength, self.y+math.cos(degToRad(self.rotation)) * self.lineLength)
    love.graphics.setColor(1,1,1,1)
end

function Player:OnKeyPress(key)

end